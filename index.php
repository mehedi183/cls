<?php
 class Person
 {
	public $name;
	public $age;
	
	public function __construct($name,$age){
		$this->name = $name;
		$this->age = $age;
	
	}
	public function PersonDetails(){
		echo "Person Name is {$this->name} and Person age is {$this->age}";
	
	}
	
 }
	$PersonOne = new Person("Mehedi","23");
	$PersonOne->PersonDetails();

?>